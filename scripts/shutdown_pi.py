#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import os


#What GPIO Pin to use ?
INT = 4

GPIO.setmode(GPIO.BCM)
GPIO.setup(INT, GPIO.IN, pull_up_down = GPIO.PUD_UP)

def countButtons(channel):
	button_count = 0
	while GPIO.input(INT)==1: 
           time.sleep(1) 
           button_count += 1
	   print ("Button count : " + str(button_count))
	if button_count>5:
	   print ("System Shutdown...")
	   os.system("sudo shutdown -h now") 
	elif button_count>2:
           print ("System Reboot...")
	   os.system("sudo reboot")

GPIO.add_event_detect(INT, GPIO.RISING, callback = countButtons, bouncetime=2000)

while 1:
     time.sleep(1)

